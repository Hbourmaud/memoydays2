import type { NextPage } from 'next'
import Head from 'next/head'
import BoardGame from '../components/BoardGame'
import styles from '../styles/Game.module.css'

const Game: NextPage = () => {
  return (
    <div >
      <Head>
        <title>Memo Game</title>
        <meta name="A Memo Game" charSet='utf-8' />
        <link rel="stylesheet"type="text/css" href="styles/global.css"/>
      </Head>
      <header>
          <div className={styles.box}>
              <div className={styles.title} ><h1>MEMO</h1></div>
          </div>
      </header>
      
      <main>
      <div>
            <div>
            <BoardGame></BoardGame>
            </div>
                <div className={styles.boxes}>
                    <div className={styles.text}>Le but est simple, retouner les cartes, les mémoriser et trouver les paires, plusieurs niveaux de difficultés sont disponibles (changer le nombre de cartes). Bonne chance!</div>
                </div>
            </div>
      </main>
    </div>
  )
}
export default Game