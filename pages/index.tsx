import type { NextPage } from 'next'
import Head from 'next/head'
import styles from '../styles/Game.module.css'

const Home: NextPage = () => {
    return (
        <div>
            <Head>
                <title>Memo Game</title>
                <meta name="A Memo Game" charSet='utf-8' />
                <link rel="stylesheet"type="text/css" href="styles/global.css"/>
            </Head>
            <header>
            <div className={styles.box}>
                <div className="titre" ><h1>MEMO</h1></div>
            </div>
            </header>
        <main>
            <div>
                <form className={styles.rect}>
                    <div className={styles.boxes}> 
                <label  htmlFor='nb_paire'>Nombre de paires: </label>
                <select className={styles.option} name='nb_paire' id='nb_paire'><option>5</option><option>10</option><option>20</option><option>30</option></select>
                </div>
                <button className={styles.boxes_button} formAction='game' type='submit'>Démarrer</button>
                </form>
            </div>
        </main>
        </div>
    )
}
    export default Home