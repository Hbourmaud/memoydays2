import { useCallback, useEffect, useState } from "react";
import Card from "./Card";
import styles from '../styles/Game.module.css'
import { CardInterface } from "../data/CardInterface";
import { generateDeck } from "../scripts/cardGame";

function delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}

const BoardGame = () => {
  const [ sentence, setSentence ] = useState<string>('La partie a commencé');
  const [ nb_card, setNbCard ] = useState<number>(10);
  const [ Cardlist, setCardList ] = useState<number[]>([]);
  const [ foundCards, setFoundCards ]  = useState<CardInterface[]>([]);
  const [ selectedCards, setSelectedCards ] = useState<CardInterface[]>([]);

  const [, updateState] = useState();
  const forceUpdate = useCallback(() => updateState(void[]), []);

  useEffect(() => {
    let search = location.search;
    const nb_paire = search.substring(1).split('&').filter((e) => e.split('=')[0] == "nb_paire")[0].split('=');
    console.log(nb_paire[1]);

    setCardList(generateDeck(nb_paire[1]));
  }, []);

  const selectCards = (card: CardInterface) => {
    (async () => {
      let temp_selected_card= selectedCards;
      temp_selected_card.push(card);
      setSelectedCards(temp_selected_card);
      if (selectedCards.length == 2) {
          if (selectedCards[0].value === selectedCards[1].value) {
              let temp_selected_card :any = foundCards;
              temp_selected_card.push(selectedCards[0]);
              temp_selected_card.push(selectedCards[1]);
              setFoundCards(temp_selected_card);
              if (foundCards.length/2 == 1){
                setSentence(foundCards.length/2+"/"+nb_card/2+" paire trouvée");
              }else{
              setSentence(foundCards.length/2+"/"+nb_card/2+" paires trouvées");
            }
          } else {
              await delay(2000);
          }
          setSelectedCards([]);
          forceUpdate();
      }
      if (foundCards.length == nb_card){
        setSentence("Bravo, toutes les paires ont été trouvées");
      }
      forceUpdate();
    })();
  }

  const checkCard = (card: CardInterface) => {
      if (selectedCards.filter((e) => e.index == card.index).length <= 0 && foundCards.filter((e: any) => e.index == card.index).length <= 0){
        selectCards(card);
      }
  }
  const displayCards = () => {
      return Cardlist.map((card, index) =>
          <Card card={card} checkCard={() => checkCard({ index: index, value: card })} index={index} selectedCards={selectedCards} foundCards={foundCards}/>
      )
  }
  return (
    <div>
      <h1>{sentence}</h1>
      <div className= {styles.rect} style={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap' }}>
          {displayCards()}
      </div>
      </div>
  );
}

export default BoardGame;