import React, { useState, useEffect, useCallback } from "react";
import Image from 'next/image'

const Card : React.FC<{ index: any, card: any , checkCard: any, selectedCards: any[] , foundCards: any[] }> = ({index, card, checkCard, selectedCards, foundCards}) => {
    const [ isFound, setIsFound ] = useState(false);
    const [ isSelected, setIsSelected ] = useState(false);
    const url_card = "/img/" + card + ".png";
    const url_back = "/img/verso.png";

    useEffect(() => {
        handleCardState();
    }, [selectedCards, foundCards])

    const handleCardState = () => {
        if (selectedCards.filter((e) => e.index == index).length > 0) {
            setIsSelected(true);
        } else {
            setIsSelected(false);
        }

        if (foundCards.filter((e) => e.index == index).length > 0) {
            setIsFound(true);
        } else {
            setIsFound(false);
        }
    }

    const handleClick = (card: { index: number, value: number }) => {
        checkCard(card);
        handleCardState();
    }
    
    return (
        <div key={index} onClick={() => {handleClick({ index: index, value: card })}} style={{ padding: '20px', margin: '20px'}}>
            <Image
            src={
                isSelected || isFound ?
                    url_card :
                    url_back
                }
            alt="Card"
            width={96}
            height={128}
            />
        </div>
    )
}

export default Card;
