# Memo Ydays
## Comment installer le projet
Nous utilisons Next.js v12.0.8 et npm v8.1.2
Cloner le projet et via une invite commande, rendez vous à l'emplacement du fichier cloné (*memoydays2*).<br/>
Pour installer et lancer le projet :<br/>
Si besoin :
```bash=
npm install
```
Ensuite pour le lancer :
```bash=
npm run dev
```
Sur votre navigateur : ```localhost:3000```

## Comment jouer
Choisisez le numéro de paire voulu (correspondant au niveau de difficulté) et cliquez sur `Démarrer`.<br/>
Il suffit ensuite de retrouver le nombre de paire en retournant les cartes et en les mémorisant.